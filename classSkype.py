from classCore import Core
import Skype4Py, random, hashlib, urllib2, json
import lxml.html as html
import datetime

class Skype:
	
	Programme = Skype4Py.Skype()
	Client = Programme.Client
	Core = Core(False)
	jokeTimer = {}
	def Listen(self, Message, Status):
		if Status == "RECEIVED":
			Text =  self.Core.Explode(Message.Body, " ")
			Chat = Message.Chat
			User = Message.FromHandle
			
			if User in self.Core.Masters:
				if Text[0] == "#addpower":
					self.addPower(Text, User, Chat)
					self.Core.Log("%s used #addpower!" % User)
				
				if Text[0] == "#removepower":
					self.removePower(Text, User, Chat)
					self.Core.Log("%s used #removepower!" % User)
			
			if User in self.Core.Powers or User in self.Core.Masters:
				if Text[0] == "#spam":
					self.Spam(Text, User, Chat)
					self.Core.Log("%s used #spam" % User)
					
				elif Text[0] == "#broadcast":
					self.Broadcast(Text, User, Chat);
					self.Core.Log("%s used #broadcast" % User);
			
			if Text[0] == "#slap":
				self.Message("Ouch %s that hurt!" % User, Chat)
				self.Message("/me slaps %s two times harder" % User, Chat)
				
			elif Text[0] == "#ping":
				self.Core.Log("Ping from %s!" % User)
				self.Message("Pong!", Chat)
			
			elif Text[0] == "#joke":
				chatterNick = Chat.Name.split("/")[0][1:]
				if chatterNick in self.jokeTimer:
					now = datetime.datetime.now()
					c = datetime.datetime.now() - self.jokeTimer[chatterNick]
					if c.seconds < 10:
						self.Message("One per 5 minutes (to prevent skid DoS attacks)", Chat)
						return
					else:
						self.jokeTimer[chatterNick] = datetime.datetime.now()
				else:
					self.jokeTimer[chatterNick] = datetime.datetime.now()
					
				url = "http://sickipedia.org/random"
				opener = urllib2.build_opener()
				opener.addheaders = [('User-agent', "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; en) Opera 9.50")]
				try:
					xsrc = html.fromstring(opener.open(url).read())
					joke = xsrc.xpath("//section[@class='jokeText']/text()")[0].lstrip()
					link = xsrc.xpath("//article[@class='jokeBox clickable']/@title")[0].strip()
					self.Message("%s\n\n%s" % (joke, link), Chat)
				except:
					self.Message("There was an error requesting a joke, try again later.", Chat)
			
			elif Text[0] == "#help":
				self.Help(User, Chat)
				
			elif Text[0] == "#powers":
				self.Powers(Chat)
				
			elif Text[0] == "#md5crypt":
				self.md5Crypt(Text, Chat)
				
			elif Text[0] == "#shortenurl":
				self.shortenUrl(Text, Chat)
				
			elif Text[0] == "#base64encode":
				self.base64Encode(Text, Chat)
				
			elif Text[0] == "#base64decode":
				self.base64Decode(Text, Chat)

	def Message(self, Message, Chat = 0, User = 0):
		if User != 0 and Chat == 0:
			Chat = self.Programme.CreateChatWith(User)
			Chat.SendMessage(Message)
		else:
			Chat.SendMessage(Message)
	
	#	Master Commands
	def addPower(self, Params, User, Chat):
		if len(Params) > 2:
			self.Message("Invalid syntax for #addpower", Chat)
			return False

		if Params[1] in self.Core.Powers:
			self.Message("%s is already a power!" % Params[1], Chat)
			return False
		
		self.Core.Powers.append(Params[1])
		self.Message("%s is now a power!" % Params[1], Chat)

	def remPower(self, Params, User, Chat):
		if len(Params) > 2:
			self.Message("Invalid syntax for #removepower", Chat)
			return False

		if Params[1] not in self.Core.Powers:
			self.Message("%s isn't a power!" % Params[1], Chat)
			return False
		
		self.Core.Powers.remove(Params[1])
		self.Message("%s is now a power!" % Params[1], Chat)

	#	Power Commands
	def Broadcast(self, Params, User, Chat):
		if len(Params) < 2:
			self.Message("Invalid syntax: #broadcast [message]", Chat)
			return False
		
		del Params[0]
		Params = self.Core.Implode(Params, " ")
		
		for friends in self.Programme.Friends:
			SkypeName = friends.Handle
			self.Message("[Broadcast from %s] %s" % (User, Params), 0, SkypeName)
		
		self.Message("Broadcast sent successfully!", Chat)

	def Spam(self, Params, User, Chat):
		if len(Params) < 3:
			self.Message("Invalid syntax: #spam [user, amount]", Chat)
			return False
		
		ToUser = Params[1]
		Amount = int(Params[2])
		Count = 0
		
		for Count in range(Amount):
			Count += 1
			self.Message("You're currently being spammed by %s! Enjoy" % (User), 0, ToUser)
			
	
	#	User Commands
	def Powers(self, Chat):
		
		self.Message("This is the current users who are Skypebot Masters and Powers:", Chat)

		self.Message("Skypebot Masters:", Chat)
		Masters =  self.Core.Implode(self.Core.Masters, ", ")
		self.Message(Masters, Chat)

		self.Message("Skypebot Powers:", Chat)
		if len(self.Core.Powers) > 0:
			Powers = self.Core.Implode(self.Core.Powers, ", ")
			self.Message(Powers, Chat)
		else:
			self.Message("None.", Chat);

	def Help(self, User, Chat):
		
		self.Message("These are the available commands for you, %s:" % User, Chat);
		
		if User in self.Core.Masters:
			for Commands in self.Core.Commands[0]:
				self.Message(Commands, Chat)
		
		if User in self.Core.Powers or User in self.Core.Masters:
			for Commands in self.Core.Commands[1]:
				self.Message(Commands, Chat)
		
		for Commands in self.Core.Commands[2]:
			self.Message(Commands, Chat)
		
	def md5Crypt(self, Params, Chat):
		toCrypt = Params[1]
		self.Message("%s encrypted with md5 is: %s" % (toCrypt, hashlib.md5(toCrypt).hexdigest()), Chat)

	def shortenUrl(self, Params, Chat):
		url = "https://www.googleapis.com/urlshortener/v1/url"
		urlToShorten = Params[1]
		if urlToShorten.find("http://") == -1 and urlToShorten.find("www.") == -1:
			self.Message("Error: Format: www.url.com OR http://url.com", Chat)
			return
		postData = {"longUrl":urlToShorten}
		req = urllib2.Request(url, json.dumps(postData), {"Content-Type":"application/json"})
		openUrl = urllib2.urlopen(req).read()
		self.Message("%s shortened becomes %s" % (urlToShorten, json.loads(openUrl)['id']), Chat)
		
	def base64Encode(self, Params, Chat):
		toEncode = Params[1]
		self.Message("%s encoded with base64 is: %s" % (toEncode, toEncode.encode('base64', 'strict')), Chat)
		
	def base64Decode(self, Params, Chat):
		toDecode = Params[1]
		self.Message("%s decoded from base64 is: %s" % (toDecode, toDecode.decode("base64")), Chat)
		
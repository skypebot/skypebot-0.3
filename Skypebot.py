from classCore import Core
from classSkype import Skype

Core = Core()
Skype = Skype()

if Skype.Programme.Attach():
	Core.Log("Couldn't attach with Skype!")
	Core.Die()

Core.Log("Listening on %s \n" % Skype.Programme.CurrentUserHandle)

Skype.Programme.OnMessageStatus = Skype.Listen

while True:
	raw_input()
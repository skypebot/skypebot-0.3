from datetime import datetime
from sys import exit
import os, random, json, urllib2

class Core:

	Masters = []

	Powers = []

	Jokes = []

	Commands = [["#addpower [Skype Name] - Makes a Skype User a power!", "#removepower [Skype Name] - Removes a Skype User from the powers!"], 
	["#spam [Skype Name, Amount] - Spams a user!", "#broadcast [Message] - Broadcasts to the entire handler's contact list!"], 
	["#joke - Grabs a joke from an API!", "#ping - Tests server-client connection!", "#slap - Slaps the robot!", "#help - Shows your available commands!", "#md5crypt [text] - Uses md5 to crypt input text", "#shortenurl [url] - Shortens a URL"]]

	def __init__(self, Run = True):
		if Run:
			if not os.path.exists('Skypebot.Admins'):
				self.Log("Masters not found!")
				
				Master = raw_input("Master Skype name: ")
				Master.lower()

				Config = open('Skypebot.Admins', 'w')
				Config.write("!%s" % Master)
			
			Config = open('Skypebot.Admins', 'r')
			Config = Config.readlines()
			
			for User in Config:
				
				User = User.strip()
				
				if "!" in User:
					User = User.strip("!");
					self.Masters.append(User)
				else:
					self.Powers.append(User)
	
	def Log(self, Message):
		print "[%s][Skypebot 0.3] %s " % (str(datetime.now().strftime("%Y-%m-%d, %H:%M:%S")), Message)

	def Explode(self, String, Seperator):
		return String.split(Seperator)
	
	def Implode(self, Array, Seperator):
		return Seperator.join(Array)

	def Die(self):
		self.Update()
		exit()